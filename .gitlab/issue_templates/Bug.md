### Description

Please describe what happened. If needed, please add some screenshots
or relevant log outputs (including relevant parts from `dmesg` and/or
`journalctl -xe` if relevant).

Additionally please add the exact command which was used to invoke `i3lock-pixeled`.

### Additional information

* OS:
* Locker script (e.g. `i3lock` or `swaylock`):
* `playerctl` version if used:
* Imagemagick version:
